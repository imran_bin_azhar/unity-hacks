﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

public class UnitsData 
{
	[XmlRoot ("collection")]
	public class UnitsXml 
	{
		[XmlElement("unit")]
		public List<BaseUnitData> units = new List<BaseUnitData>();
	}

	[XmlRoot ("collection")]
	public class CastleXml 
	{
		[XmlElement("castle")]
		public List<BaseCastleData> castles = new List<BaseCastleData>();
	}

	private static UnitsXml heroesBase = new UnitsXml();
	private static UnitsXml soldiersBase = new UnitsXml();
	private static UnitsXml creaturesBase = new UnitsXml();

	private static UnitsXml heroesUpgrade = new UnitsXml();
	private static UnitsXml soldiersUpgrade = new UnitsXml();
	private static UnitsXml creaturesUpgrade = new UnitsXml();

	private static CastleXml castleBase = new CastleXml();

	public static void GetAllData()
	{
		ReadHeroesData ();
		ReadCreaturesData ();
		ReadSoldiersData ();

		ReadHeroesUpgrade ();
		ReadCreaturesUpgrade ();
		ReadSoldiersUpgrade ();

		ReadCastleData ();
		//Debug.Log ("dfdff");
	}

	private static void ReadHeroesData()
	{
		
		var serializer = new XmlSerializer(typeof(UnitsXml));
		heroesBase = serializer.Deserialize(PathToStream("Data/Heroes")) as UnitsXml;
	}

	private static void ReadCreaturesData()
	{
		var serializer = new XmlSerializer(typeof(UnitsXml));
		creaturesBase = serializer.Deserialize(PathToStream("Data/Creatures")) as UnitsXml;
	}

	private static void ReadSoldiersData()
	{
		var serializer = new XmlSerializer(typeof(UnitsXml));
		soldiersBase = serializer.Deserialize(PathToStream("Data/Soldiers")) as UnitsXml;
	}

	private static void ReadHeroesUpgrade()
	{
		var serializer = new XmlSerializer(typeof(UnitsXml));
		heroesUpgrade = serializer.Deserialize(PathToStream("Data/HeroesUpgrade")) as UnitsXml;
	}

	private static void ReadCreaturesUpgrade()
	{
		var serializer = new XmlSerializer(typeof(UnitsXml));
		creaturesUpgrade = serializer.Deserialize(PathToStream("Data/CreaturesUpgrade")) as UnitsXml;
	}

	private static void ReadSoldiersUpgrade()
	{
		var serializer = new XmlSerializer(typeof(UnitsXml));
		soldiersUpgrade = serializer.Deserialize(PathToStream("Data/SoldiersUpgrade")) as UnitsXml;

	}

	private static void ReadCastleData()
	{
		var serializer = new XmlSerializer(typeof(CastleXml));
		castleBase = serializer.Deserialize(PathToStream("SavedData/Castles")) as CastleXml;

	}

	public static MemoryStream PathToStream(string givenPath){
		TextAsset ta =  Resources.Load<TextAsset> (givenPath);
		byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(ta.text);
		MemoryStream stream = new MemoryStream(byteArray);
		return stream;
	}

	public static BaseUnitData GetHeroeById(int id)
	{
		return heroesBase.units.Find (x => x.id == id);
	}

	public static BaseUnitData GetSoldierById(int id)
	{
		return soldiersBase.units.Find (x => x.id == id);
	}

	public static BaseUnitData GetCreatureById(int id)
	{
		return creaturesBase.units.Find (x => x.id == id);
	}

    public static BaseUnitData GetRandomHero()
    {
        return heroesBase.units[UnityEngine.Random.Range(0, heroesBase.units.Count)];
    }

    public static BaseUnitData GetRandomCreature()
    {
        return creaturesBase.units[UnityEngine.Random.Range(0, creaturesBase.units.Count)];
    }
}