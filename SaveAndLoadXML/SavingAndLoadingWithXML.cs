﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;
using System.Linq;

//Script Number 1 (Save and Load data)

//Sample save file is in the same directory by the name of checkSave.xml

//Script for saving and loading data to .xml file type and save to .xml file type
/// <summary>
/// This script will save following things:
/// (a). Is the game saved? 
/// (b):
/// 1. Player
/// 2. Castle information
/// 3. Computer information
/// 4. Armies including its heros and creatures each hero owns
/// 5. Stable in which all creatures are kept
/// 6. to be continued...
/// </summary>


public class GameSaveLoadSc : MonoBehaviour {



	void Awake(){
		
	}

	// Use this for initialization
	void Start () {
		
	}
	//This function will save data on its call
	public void SaveIt(){
		SaveContainer monsterCollection2 = new SaveContainer ();
		monsterCollection2.SaveGame();
	}

	//This function will load data on its call
	public void LoadIt(){
		var cont = SaveContainer.Load (Path.Combine (Application.dataPath, "checkSave.xml"));
//		Debug.Log ("dsafadsfdas : " + cont.castles[0].owner);
		cont.LoadGame ();
	}

	// Update is called once per frame
	void Update () {
		
	}
}


//A class is needed to save data in one file
[XmlRoot("SavedGame")] //This will be the root tag of the saved file. Everything will be its chiled
public class SaveContainer{
	
	public GameState state = GameState.Organize;
	public Player player;

	[XmlArray("Castles")]//This will save this data as an array and will help in loading also
	public List<SaveCastle> castles = new List<SaveCastle> ();

	[XmlArray("Computers")]
	public List<Player> computer = new List<Player>();

	[XmlArray("Armies")]
	public List<SaveArmy> armies = new List<SaveArmy> ();

	[XmlArray("Stable")]
	public List<Unit> stable = new List<Unit>();


	public void CopyCastleData(){
		castles.Clear ();
		foreach (Castle ca in Game.castles) {
			SaveCastle temp = new SaveCastle ();
			temp.id = ca.id;
			temp.level = ca.level;
			temp.owner = ca.Owner;
			castles.Add (temp);
		}
	}

	public void CopyComputers(){
		foreach(Player pl in Game.computers){
			Player temp = new Player ();
			temp = pl;
			computer.Add (temp);
		}
	}

	public void CopyArmies(){
		foreach (Army ar in Game.armies) {
			SaveArmy temp = new SaveArmy ();
			temp.castleId = ar.castleId;
			temp.id = ar.id;
			temp.owner = ar.owner;
			temp.hero = ar.hero;
			temp.isUnique = ar.isUnique;
			foreach (Unit un in ar.creatures) {
				Unit tempUnit = new Unit ();
				tempUnit = un;
				temp.creatures.Add (tempUnit);
			}
			armies.Add (temp);
		}
	}

	public void CopyStables(){
		foreach (Unit un in Game.stables) {
			Unit temp = new Unit ();
			temp = un;
			stable.Add (temp);
		}
	}

	public void SaveGame()
	{
		state = Game.state;
		player = Game.player;
		CopyCastleData ();
		CopyComputers ();
		CopyArmies ();
		CopyStables ();
		//This is how the game is saved at specified path
		//Application.datapath directs to the assets directory
		Save (Path.Combine(Application.dataPath, "checkSave.xml"));
	}

	public void Save(string path)
	{
		//It should use same class in which its declared
		var serializer = new XmlSerializer(typeof(SaveContainer));
		using(var stream = new FileStream(path, FileMode.Create))
		{
			serializer.Serialize(stream, this);
		}
	}

	public void LoadGame(){
		//Load(Path.Combine(Application.dataPath, "checkSave.xml"));


		PasteCastleData ();
		PasteComputersData ();
		PasteArmiesData ();
		PasteStableData ();
	}

	public static SaveContainer Load(string path)
	{
		var serializer = new XmlSerializer(typeof(SaveContainer));
		using(var stream = new FileStream(path, FileMode.Open))
		{
			return serializer.Deserialize(stream) as SaveContainer;//It will load the data in its own class variables
		}
	}

	public void PasteCastleData(){
		Game.castles.Clear ();
		Game.castles = GameObject.FindObjectsOfType<Castle> ().OrderBy (x => x.id).ToList ();
		for (int i = 0; i < castles.Count; i++) {
			if (castles [i] != null && Game.castles [i] != null) {
				Game.castles [i].ChangeLevel (castles [i].level);
				Game.castles [i].ChangeOwner (castles [i].owner);
			}
		}
	}

	public void PasteComputersData(){
		Game.computers.Clear ();
		foreach(Player pl in computer){
			Player temp = new Player ();
			temp = pl;
			Game.computers.Add (temp);
		}
	}

	public void PasteArmiesData(){
		Game.armies.Clear ();
		foreach (SaveArmy ar in armies) {
			Army temp = new Army (ar.id, ar.owner, ar.hero.id, ar.isUnique, ar.castleId);
			temp.hero = ar.hero;
			foreach (Unit un in ar.creatures) {
				Unit tempUnit = new Unit ();
				tempUnit = un;
				temp.creatures.Add (tempUnit);
			}
			Game.armies.Add (temp);
		}
	}
	public void PasteStableData(){
		Game.stables.Clear ();
		foreach (Unit un in stable) {
			Unit temp = new Unit ();
			temp = un;
			Game.stables.Add (temp);
		}
	}
}

public class SaveGame: Game{
	
}

public class SaveCastle{
	public int id;
	public int level;
	public Nation owner;
}

public class SaveArmy{
	public int id;
	public int castleId;
	public Nation owner;
	public Unit hero;
	public List<Unit> creatures = new List<Unit>();
	public bool isUnique;
}