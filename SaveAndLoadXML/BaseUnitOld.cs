﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;

public class BaseUnitData
{
	[XmlAttribute("id")] public int id;
	[XmlAttribute("name")] public string name;
    public string displayName;
	[XmlAttribute("leader")] public bool leader;
	[XmlElement] public string description;
	[XmlElement] public int health;
	[XmlElement] public int mana;
	[XmlElement] public int manaUpkeep;
	[XmlElement] public int strength;
	[XmlElement] public int intelligence;
	[XmlElement] public int defence;
	[XmlElement] public int runePower;
	[XmlElement] public int runeNeeded;
	[XmlElement] public int agility;
	[XmlElement] public int movement;
	[XmlElement] public int range;
	[XmlElement] public int energy;
	[XmlElement] public int experience;
	[XmlElement] public int Level = 1;
	[XmlElement] public List<UpgradeSystem> upgradableTo = new List<UpgradeSystem>();
	[XmlEnum] public UnitType unitType;
	[XmlElement] public bool isParentUnit = false;
	[XmlElement] public int myTier = 1;
	[XmlElement] public int parentUnitID;
	[XmlElement] public bool isLocked = true;
	[XmlElement] public int unlockLevel;
}

public class BaseCastleData
{
	[XmlAttribute("id")] public int id;
	[XmlElement] public int castleLevel;

	
}
	
