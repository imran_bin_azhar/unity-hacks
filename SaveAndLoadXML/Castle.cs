﻿using UnityEngine;
using System;
using System.Collections;

public class Castle : MonoBehaviour
{
	[Serializable]
	public class LevelsData
	{
		public int[] soldierIds;
		public int[] creatureIds;
		public int income;
		public int price;
		public string[] bonuses;
		public Sprite[] factionSprites;
		public Color[] factionColors;
	}

	private SpriteRenderer border;
	private TweenAlpha talpha;
	private SpriteRenderer castleSprite;
	private Nation owner = Nation.Carnifair;

	public int id;
	public int mapId;
	public UILabel nameLabel;
	public SpriteRenderer hasArmy;
	public SpriteRenderer hasMove;
	public SpriteRenderer hasAttack;
	public int level;
	public int[] neighbours;
   
	public LevelsData[] levelsData;

	public Nation Owner {
		get { return owner; }

	}

	void Awake ()
	{
		border = transform.parent.GetComponent<SpriteRenderer> ();
		talpha = GetComponent<TweenAlpha> ();
		border.color = Color.white;
		castleSprite = GetComponent<SpriteRenderer> ();
	}

	public void EnableGlow ()
	{
		talpha.enabled = true;

		nameLabel.text = name + " " + id;
	}


	public void DisableGlow ()
	{
		talpha.enabled = false;
		nameLabel.text = "";
		castleSprite.color = Color.white;

	}

	public void LevelUp ()
	{
//		if (owner != Game.player.faction)
//			return;
//
//		if (Game.player.gold < levelsData [level + 1].price)
//			return;
//
//		Game.player.gold -= levelsData [level + 1].price;
		level++;
		level = Mathf.Min (level + 1, 9);
		castleSprite.sprite = levelsData [level].factionSprites [(int)owner];
	}

	public void ChangeLevel(int levelToChange)
	{
		while (level < levelToChange)
			LevelUp ();
	}

	public string GetBonusString ()
	{
		string bonus = "Bonus:\n\r";
		for (int i = 0; i < levelsData [level].bonuses.Length; i++) {
			bonus += levelsData [level].bonuses [i] + "\n\r";
		}
		return bonus;
	}

	public string GetIncomeString ()
	{
		return levelsData [level].income + "/" + levelsData [level].income;
	}

	public int GetIncome ()
	{
		return levelsData [level].income;
	}

	public bool isPlayer ()
	{
		return owner == Game.player.faction;
	}

	public void ChangeOwner (Nation faction)
	{
		owner = faction;
		border.color = levelsData [level].factionColors [(int)owner];
		castleSprite.sprite = levelsData [level].factionSprites [(int)owner];
	}
}