﻿using UnityEngine;
using System.Collections;

public enum Difficulty
{
	Easy,
	Medium,
	Hard
}

public class Player 
{
	public Difficulty difficulty = Difficulty.Medium;
	public Nation faction;
	public int gold = 100;
	public int mana = 100;

	public Player(){
		
	}

	public Player(Nation faction)
	{
		this.faction = faction;
	}
}