﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

public class Unit : BaseUnitData 
{
	public int level;
	public int castleId;
	public int currentHealth;
    	public int currentMana;
	public int currentExperience;
	[XmlIgnore]
    	public List<Spell> mySpells = new List<Spell>();
	[XmlIgnore]
    	public float HpPercentage { get { return ((float)currentHealth / (float)health); } }
	[XmlIgnore]
	public Dictionary<string, int> defaultModifiedAttribute = new Dictionary<string, int>();

    	public Unit(BaseUnitData unit)
	{
		id = unit.id;
		name = unit.name;
        	displayName = unit.name;
        	leader = unit.leader;
		description = unit.description;
		health = unit.health;
		mana = unit.mana;
		manaUpkeep = unit.manaUpkeep;
		strength = unit.strength;
		intelligence = unit.intelligence;
		defence = unit.defence;
		runePower = unit.runePower;
		runeNeeded = unit.runeNeeded;
		agility = unit.agility;
		movement = unit.movement;
		range = unit.range;
		energy = unit.energy;
		experience = unit.experience;
		unitType = unit.unitType;
		isParentUnit = unit.isParentUnit;
		myTier = unit.myTier;
		parentUnitID = unit.parentUnitID;
		isLocked = unit.isLocked;
		unlockLevel = unit.unlockLevel;

		level = 1;
		currentHealth = health;
		currentExperience = 0;
		this.upgradableTo.AddRange (unit.upgradableTo);
	}

	public Unit(){
		
	}

    	public void AddExperience()
	{
		
	}

	public void ChangeHealth()
	{
		
	}
}